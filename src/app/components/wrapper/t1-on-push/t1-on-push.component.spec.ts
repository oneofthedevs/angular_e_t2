import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T1OnPushComponent } from './t1-on-push.component';

describe('T1OnPushComponent', () => {
  let component: T1OnPushComponent;
  let fixture: ComponentFixture<T1OnPushComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T1OnPushComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T1OnPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
