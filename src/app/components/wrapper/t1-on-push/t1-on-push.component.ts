import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 't1-on-push',
  templateUrl: './t1-on-push.component.html',
  styleUrls: ['./t1-on-push.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class T1OnPushComponent implements OnInit {
  @Input() text: string = '';
  @Input() arr: string[] = [];
  constructor(private _changeDetector: ChangeDetectorRef) {}

  ngOnInit(): void {}

  public detectChanges(): void {
    this._changeDetector.detectChanges();
  }
}
