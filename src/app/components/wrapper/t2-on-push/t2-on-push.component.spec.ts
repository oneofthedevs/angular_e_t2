import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T2OnPushComponent } from './t2-on-push.component';

describe('T2OnPushComponent', () => {
  let component: T2OnPushComponent;
  let fixture: ComponentFixture<T2OnPushComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T2OnPushComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T2OnPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
