import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 't1-default',
  templateUrl: './t1-default.component.html',
  styleUrls: ['./t1-default.component.scss'],
})
export class T1DefaultComponent implements OnInit {
  @Input() text: string = '';
  @Input() arr: string[] = [];

  constructor() {}

  ngOnInit(): void {}
}
