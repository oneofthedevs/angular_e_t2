import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T1DefaultComponent } from './t1-default.component';

describe('T1DefaultComponent', () => {
  let component: T1DefaultComponent;
  let fixture: ComponentFixture<T1DefaultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T1DefaultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T1DefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
