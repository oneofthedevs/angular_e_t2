import { ComponentFixture, TestBed } from '@angular/core/testing';

import { T2DefaultComponent } from './t2-default.component';

describe('T2DefaultComponent', () => {
  let component: T2DefaultComponent;
  let fixture: ComponentFixture<T2DefaultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ T2DefaultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(T2DefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
