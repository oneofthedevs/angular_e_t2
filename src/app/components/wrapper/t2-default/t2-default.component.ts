import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, interval } from 'rxjs';
import { RandomService } from 'src/app/services/random/random.service';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 't2-default',
  templateUrl: './t2-default.component.html',
  styleUrls: ['./t2-default.component.scss'],
})
export class T2DefaultComponent implements OnInit {
  normalValue: Number | undefined;
  obsValue: BehaviorSubject<Number> | undefined;
  obsInterval: any = interval(5000);
  intervalValue: Number | undefined;

  constructor(private _randomService: RandomService) {}

  ngOnInit(): void {
    this.callInterval();
    this.subscribeToRandomValue();
    this.getRandomObsObservable();
    this.subscribeInterval();
  }

  /**
   * @description Calling setInterval function
   */
  private callInterval(): void {
    setInterval(() => {
      console.log('Task 2: Default Component Interval called');
    }, 3000);
  }

  /**
   * @description Subscribing to behaviourSubject from service
   */
  private subscribeToRandomValue(): void {
    this._randomService.obs.pipe(untilDestroyed(this)).subscribe((x) => {
      this.normalValue = x;
    });
  }

  /**
   * @description Returning behaviourSubject from service so it can be used with async pipe
   */
  private getRandomObsObservable(): void {
    this.obsValue = this._randomService.obs;
  }

  /**
   * @description Subscribing to interval (initialized on Line no. 13)
   */
  private subscribeInterval(): void {
    this.obsInterval.pipe(untilDestroyed(this)).subscribe((x: any) => {
      this.intervalValue = x;
    });
  }
}
