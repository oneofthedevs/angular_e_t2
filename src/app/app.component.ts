import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'changeDetectionAssignment';
  /***
   ** Assignment is divided into 2 tasks
   ** Task 1: OnPush strategy and the mutability trap.
   ** Task 2: OnPush strategy with Observable, async pipe and Interval()
   *
   ** Here wrapper is the parent component
   ** t1-default and t1-on-push are child components with default and onpush changedetection respectively and are used to show output of task 1
   ** t2-default and t2-on-push are similar to their t1 counterparts and are used to show output of task 2
   */
}
